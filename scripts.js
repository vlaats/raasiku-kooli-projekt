var myList = document.getElementsByTagName("LI")
var i // index

for (i = 0; i < myList.length; i++) {
    var span = document.createElement("SPAN")
    var closeBtn = document.createTextNode("\u00D7")
    span.className = "close"
    span.appendChild(closeBtn)
    myList[i].appendChild(span)
}

var input = document.getElementById("todoInput")

input.addEventListener("keyup", function(event) {
    if (event.key === "Enter") {
        // Prevent to reload the page
        event.preventDefault()
        // Call a function to add a todo
        addNewTodo()
    }
})

// Click on a close button to hide the current list item
var close = document.getElementsByClassName("close")
var i
for (i = 0; i < close.length; i++) {
    close[i].onclick = function() {
        var div = this.parentElement
        div.style.display = "none"
    }
}

// Fuction to add new todo
function addNewTodo() {

    // create a new list item
    var listItem = document.createElement("li")

    // get input
    var inputValue = document.getElementById("todoInput")

    // create a new text node element
    var textNode = document.createTextNode(inputValue.value)

    // append list item to node element
    listItem.appendChild(textNode)

    if (inputValue.value === "") {
        alert("You must write something!!!!")
    } else {
        listItem.className = "list-item"
        document.getElementById("todoList").appendChild(listItem)
    }

    // clear input value
    inputValue.value = ""
    
    // create new span element
    var span = document.createElement("SPAN")

    // close button
    var closeBtn = document.createTextNode("\u00D7")

    // add close class to span
    span.className = "close"

    // add close to list item
    span.appendChild(closeBtn)

    // add span to list item
    listItem.appendChild(span)

    for (i = 0; i < close.length; i++) {
        close[i].onclick = function() {
            var div = this.parentElement
            div.style.display = "none"
        }
    }
    
}